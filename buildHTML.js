#!/usr/bin/env node

'use strict';

const fs = require( 'fs' );
const path = require( 'path' );
const pp = require( 'preprocess' );

const from = "./src/index.html";
const to = "./dist/index.html";

var fromPath = fs.realpathSync(from);
var toPath = fs.realpathSync(to);

fs.stat( fromPath, function( error, stat ) {
    if( error ) {
        console.error( "Error stating file.", error );
        return;
    }

    if( stat.isFile() ) {
        pp.preprocessFileSync(fromPath, toPath, {});
    }
} );
